<?php

namespace Yarsha\MediaManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Yarsha\MediaManagerBundle\Entity\Post;
use Yarsha\MediaManagerBundle\Form\PostType;


class PostController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/post/list", name="yarsha_post_list")
     */
    public function listPostAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Post::class)->findAll();
        $data['posts'] = $post;
        return $this->render('@YarshaMediaManager/post/list.html.twig', $data);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/post/add", name="yarsha_post_add")
     * @Route("/post/{id}/edit", name="yarsha_post_edit")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $mediaService = $this->get('yarsha.service.media_manager');
        $em = $this->getDoctrine()->getManager();
        $data['medias'] = $mediaService->getAllMedia();
        if(!$id){
            $post = new Post();
            $post->setCreatedAt(new \DateTime('now'));
            $data['isUpdating'] = false;
        }   else {
            $data['isUpdating'] = true;
            $post = $mediaService->getPostById($id);
            $post->setUpdatedAt(new \DateTime('now'));
            if(!$post){
                return $this->redirectToRoute('yarsha_post_list');
            }
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if($form->isValid() and $form->isSubmitted()){
            $post = $form->getData();
            $em->persist($post);
            try{
                $em->flush();
                if($data['isUpdating']){
                    $this->addFlash('success', 'Post updated.');
                }   else    {
                    $this->addFlash('success', 'Post added.');
                }
            }   catch (\Throwable $e){
                $this->addFlash('error', 'Something went wrong. Unable to add gallery.');
            }
            return $this->redirectToRoute('yarsha_post_list');
        }
        $data['form'] = $form->createView();
        return $this->render('@YarshaMediaManager/post/add.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/post/{id}/delete", name="yarsha_media_manager_post_delete")
     */
    public function deletePostAction(Request $request){
        $id = $request->get('id');
        if(!$id){
            return $this->redirectToRoute('yarsha_post_list');
        }
        $mediaService = $this->get('yarsha.service.media_manager');
        $em = $this->getDoctrine()->getManager();
        $post = $mediaService->getPostById($id);
        if(!$post){
            return $this->redirectToRoute('yarsha_post_list');
        }

        $em->remove($post);

        try{
            $em->flush();
            $this->addFlash('success', 'Post deleted.');
        }   catch (\Exception $e){
            $this->addFlash('error', $e->getMessage());
        }
        return $this->redirectToRoute('yarsha_post_list');

    }

}
