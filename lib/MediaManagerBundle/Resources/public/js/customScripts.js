$('.delete').on('click', function(){
    var $this = $(this);
    bootbox.confirm({
        size:'small',
        message: 'Are you sure ?',
        callback: function(result){
            if(result){
                var href = $this.attr('href');
                window.location.href = href;
            }
        }
    });
    return false;
});